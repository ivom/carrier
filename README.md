# Carrier

Deliver messages from transactional outbox reliably to all destination systems using declarative configuration.

## Development

### Prerequisites

- Docker
- Docker Compose v2.17+

### Build

Build Docker image:

```shell
docker build .
```

Build JAR file only:

```shell
mvnw
```

### Run

Rebuild and run:

```shell
docker compose build && docker compose up
```

Then, in another terminal, watch sources and rebuild on changes:

```shell
docker compose alpha watch
```

### Run integration tests

#### Cmd line

```shell
mvnw -Pit
```

#### IDEA

To be able to run tests from IDEA, perform the following setup:

Run -> Edit configurations... -> Edit configuration templates... -> JUnit -> Modify options...
-> Add before launch task -> Run Maven Goal ->
Command line = `process-test-resources -Dmaven.main.skip -Dmaven.resources.skip`

### Re-create database test support files

Run after `src/test/resources/db/recreate-db.sql` has been changed.

```shell
mvnw -Pdb
```
