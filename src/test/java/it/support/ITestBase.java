package it.support;

import net.sf.lightair.LightAir;
import org.joda.time.DateTimeZone;
import org.junit.runner.RunWith;

import java.time.ZoneOffset;
import java.util.TimeZone;

@RunWith(LightAir.class)
public abstract class ITestBase {

	static {
		TimeZone.setDefault(TimeZone.getTimeZone(ZoneOffset.UTC));
		DateTimeZone.setDefault(DateTimeZone.UTC);
	}
}
