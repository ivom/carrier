drop table if exists outbox_minimal;

create table outbox_minimal (
  id   uuid primary key,
  data text
);
