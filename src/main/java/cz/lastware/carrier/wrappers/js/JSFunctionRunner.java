package cz.lastware.carrier.wrappers.js;

import lombok.extern.slf4j.Slf4j;
import org.openjdk.nashorn.api.scripting.ScriptObjectMirror;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.Map;

@Slf4j
public class JSFunctionRunner {

	public static void run() {
		ScriptEngine engine = new ScriptEngineManager().getEngineByName("JavaScript");
		log.info("engine {}", engine);
		try {
			Map<String, Object> context = Map.of(
					"entityId", 1234,
					"entityName", "User"
			);
			var statement =
					"function (context) { return { url: '/users/' + context.entityId, name: context.entityName } }";
			var fn = (ScriptObjectMirror) engine.eval(statement);
			var result = (ScriptObjectMirror) fn.call(null, context);
			log.info("Result class {}, url {}, name {}, {}",
					result.getClass().getName(), result.get("url"), result.get("name"), result);
		} catch (ScriptException exception) {
			log.error("Exception", exception);
		}
	}
}
