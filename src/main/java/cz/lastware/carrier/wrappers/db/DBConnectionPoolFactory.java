package cz.lastware.carrier.wrappers.db;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.util.Map;
import java.util.Properties;

public class DBConnectionPoolFactory {

	public static HikariDataSource create(Map<String, ?> source) {
		var properties = new Properties();
		properties.putAll(source);
		var hikariConfig = new HikariConfig(properties);
		return new HikariDataSource(hikariConfig);
	}
}
