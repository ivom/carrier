package cz.lastware.carrier.wrappers.validation;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class Validation {

	private final Validator validator;

	public static List<ValidationError> toValidationErrors(Set<? extends ConstraintViolation<?>> constraintViolations) {
		return constraintViolations.stream()
				.map(violation -> new ValidationError(
						violation.getPropertyPath().toString(),
						violation.getInvalidValue(),
						violation.getMessage()))
				.sorted()
				.toList();
	}

	/**
	 * Perform bean validations (JSR-303).
	 * <p>
	 * Calls verify method to stop processing.
	 *
	 * @param data   bean to be validated, annotated with JSR-303 annotations
	 * @param groups the group or list of groups targeted for validation
	 * @param <T>    type of bean data to verify
	 */
	public <T> void verifyBean(T data, Class<?>... groups) {
		Set<ConstraintViolation<T>> constraintViolations = validator.validate(data, groups);
		List<ValidationError> validationErrors = toValidationErrors(constraintViolations);
		verify(validationErrors);
	}

	/**
	 * Verify that no errors have been collected during previous validations.
	 * <p>
	 * If there are any errors, throw {@link ValidationException}.
	 *
	 * @param validationErrors validation errors
	 */
	public void verify(List<ValidationError> validationErrors) {
		if (!validationErrors.isEmpty()) {
			throw new ValidationException(validationErrors);
		}
	}
}
