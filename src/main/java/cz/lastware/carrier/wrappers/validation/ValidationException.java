package cz.lastware.carrier.wrappers.validation;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Collections;
import java.util.List;

/**
 * Validation exception.
 * <p>
 * Contains a list of {@link ValidationError}s.
 */
@AllArgsConstructor
@Getter
public class ValidationException extends RuntimeException {

	private final List<ValidationError> validationErrors;

	public ValidationException(ValidationError validationError) {
		this.validationErrors = Collections.singletonList(validationError);
	}

	public ValidationException(String path, Object value, String errorCode) {
		this(new ValidationError(path, value, errorCode));
	}

	public ValidationException(String errorCode) {
		this(new ValidationError(null, null, errorCode));
	}

	@Override
	public String getMessage() {
		return "Validation failure: " + validationErrors;
	}
}
