package cz.lastware.carrier.wrappers.validation;

public record ValidationError(
		String path,
		Object value,
		String errorCode
) implements Comparable<ValidationError> {

	@Override
	public int compareTo(ValidationError o) {
		int byPath = path.compareTo(o.path);
		int byErrorCode = errorCode.compareTo(o.errorCode);
		return (0 == byPath) ? byErrorCode : byPath;
	}
}
