package cz.lastware.carrier.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.representer.Representer;

@Configuration
public class YamlConfig {

	@Bean
	public Representer representer() {
		var representer = new Representer(new DumperOptions());
		representer.getPropertyUtils().setSkipMissingProperties(true);
		return representer;
	}

	@Bean
	public Yaml yaml() {
		return new Yaml(representer());
	}
}
