package cz.lastware.carrier.config;

import com.zaxxer.hikari.HikariDataSource;
import cz.lastware.carrier.app.configuration.ConfigurationLoader;
import cz.lastware.carrier.app.configuration.dto.Configuration;
import cz.lastware.carrier.wrappers.db.DBConnectionPoolFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class ApplicationEventListener {

	@Value("${spring.application.name}")
	private String appName;

	@EventListener(ApplicationReadyEvent.class)
	public void onStarted() {
		log.info("{} initializing...", appName);
		initialize();
		log.info("{} initialized.", appName);
	}

	@EventListener(ContextClosedEvent.class)
	public void onStopped() {
		log.info("{} stopping...", appName);
		shutdown();
		log.info("{} stopped.", appName);
	}

	private final ConfigurationLoader configurationLoader;

	private HikariDataSource dataSource = null;

	private void initialize() {
		Configuration configuration = configurationLoader.load()
				.orElse(null);
		if (configuration != null) {
			dataSource = DBConnectionPoolFactory.create(configuration.getSource());
			var jdbcTemplate = new JdbcTemplate(dataSource);
			log.info("jdbcTemplate {}", jdbcTemplate);
		}
	}

	private void shutdown() {
		if (dataSource != null) {
			dataSource.close();
		}
	}
}
