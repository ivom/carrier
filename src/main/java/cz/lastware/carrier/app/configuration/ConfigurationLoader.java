package cz.lastware.carrier.app.configuration;

import cz.lastware.carrier.app.configuration.dto.Configuration;
import cz.lastware.carrier.wrappers.validation.Validation;
import cz.lastware.carrier.wrappers.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.error.YAMLException;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class ConfigurationLoader {

	public static final String PATH = "/app/carrier.yaml";

	private final Yaml yaml;
	private final Validation validation;

	public Optional<Configuration> load() {
		try (var is = new FileInputStream(PATH)) {
			Configuration configuration = yaml.loadAs(is, Configuration.class);
			validation.verifyBean(configuration);
			log.info("Loaded: {}", configuration);
			return Optional.of(configuration);
		} catch (IOException e) {
			log.warn("Error loading configuration from path {}: {}", PATH, e.toString());
			log.info("Have you mapped your carrier.yaml file via a Docker volume to this path?");
		} catch (YAMLException e) {
			log.error("Error parsing configuration", e);
		} catch (ValidationException e) {
			log.error("Error validating configuration: {}", e.toString());
		}
		log.warn("No valid configuration loaded.");
		return Optional.empty();
	}
}
