package cz.lastware.carrier.app.configuration.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.Map;

@Data
public class Configuration {

	@NotNull
	private Map<String, Object> source;
}
