FROM maven:3.9.4-eclipse-temurin-20-alpine AS build
WORKDIR /app
COPY pom.xml .
COPY src ./src
# build the app (no dependency download here)
RUN --mount=type=cache,target=/root/.m2 mvn clean package -Dmaven.test.skip

FROM eclipse-temurin:20.0.2_9-jre-alpine
RUN mkdir /app
RUN addgroup --system carrier && adduser --system --ingroup carrier carrier
COPY --from=build /app/target/carrier.jar /app
WORKDIR /app
RUN chown -R carrier:carrier /app
USER carrier:carrier
EXPOSE 48813
ENTRYPOINT ["java","-jar","carrier.jar"]
